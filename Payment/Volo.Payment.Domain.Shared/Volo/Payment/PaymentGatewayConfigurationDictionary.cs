﻿using System.Collections.Generic;

namespace Volo.Payment
{
    public class PaymentGatewayConfigurationDictionary : Dictionary<string, PaymentGatewayConfiguration>
	{
		public void Add(PaymentGatewayConfiguration gatewayConfiguration)
		{
			base[gatewayConfiguration.Name] = gatewayConfiguration;
		}
	}
}
