﻿using System;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;

namespace Volo.Abp.LanguageManagement
{
    public class LanguageText : AuditedEntity<Guid>, IEntity<Guid>, IAggregateRoot<Guid>, IMultiTenant, IAggregateRoot, IEntity
	{
		public virtual Guid? TenantId { get; protected set; }

		public virtual string ResourceName { get; set; }

		public virtual string CultureName { get; set; }

		public virtual string Name { get; set; }

		public virtual string Value { get; set; }

		public LanguageText(Guid id, string resourceName, string cultureName, string name, string value = null, Guid? tenantId = null)
		{
			Check.NotNullOrWhiteSpace(resourceName, "resourceName", int.MaxValue, 0);
			Check.NotNullOrWhiteSpace(cultureName, "cultureName", int.MaxValue, 0);
			Check.NotNullOrWhiteSpace(name, "name", int.MaxValue, 0);
			this.Id = id;
			this.ResourceName = resourceName;
			this.CultureName = cultureName;
			this.Name = name;
			this.Value = value;
			this.TenantId = tenantId;
		}
	}
}
