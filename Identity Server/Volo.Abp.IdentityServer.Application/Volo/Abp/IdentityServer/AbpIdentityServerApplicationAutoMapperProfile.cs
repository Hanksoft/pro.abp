﻿using System;
using AutoMapper;
using Volo.Abp.Identity;
using Volo.Abp.IdentityServer.ApiResources.Dtos;
using Volo.Abp.IdentityServer.ApiResources;
using Volo.Abp.IdentityServer.ClaimTypes.Dtos;
using Volo.Abp.IdentityServer.Clients.Dtos;
using Volo.Abp.IdentityServer.Clients;
using Volo.Abp.IdentityServer.IdentityResources.Dtos;
using Volo.Abp.IdentityServer.IdentityResources;

namespace Volo.Abp.IdentityServer
{
	public class AbpIdentityServerApplicationAutoMapperProfile : Profile
	{
		public AbpIdentityServerApplicationAutoMapperProfile()
		{
			AbpAutoMapperExtensibleDtoExtensions.MapExtraProperties<IdentityResource, IdentityResourceWithDetailsDto>(base.CreateMap<IdentityResource, IdentityResourceWithDetailsDto>());
			base.CreateMap<IdentityResources.IdentityClaim, IdentityClaimDto>();
			base.CreateMap<ApiResourceClaim, ApiResourceClaimClaimDto>();
			AbpAutoMapperExtensibleDtoExtensions.MapExtraProperties<ApiResource, ApiResourceWithDetailsDto>(base.CreateMap<ApiResource, ApiResourceWithDetailsDto>());
			base.CreateMap<ApiScope, ApiScopeDto>();
			base.CreateMap<ApiSecret, ApiSecretDto>();
			base.CreateMap<ApiScopeClaim, ApiScopeClaimDto>();
			AbpAutoMapperExtensibleDtoExtensions.MapExtraProperties<Client, ClientWithDetailsDto>(base.CreateMap<Client, ClientWithDetailsDto>());
			base.CreateMap<ClientSecret, ClientSecretDto>();
			base.CreateMap<ClientScope, ClientScopeDto>();
			base.CreateMap<ClientClaim, ClientClaimDto>();
			base.CreateMap<ClientProperty, ClientPropertyDto>();
			base.CreateMap<ClientRedirectUri, ClientRedirectUriDto>();
			base.CreateMap<ClientPostLogoutRedirectUri, ClientPostLogoutRedirectUriDto>();
			base.CreateMap<ClientIdPRestriction, ClientIdentityProviderRestrictionDto>();
			base.CreateMap<ClientGrantType, ClientGrantTypeDto>();
			base.CreateMap<ClientCorsOrigin, ClientCorsOriginDto>();
			AbpAutoMapperExtensibleDtoExtensions.MapExtraProperties<IdentityClaimType, IdentityClaimTypeDto>(base.CreateMap<IdentityClaimType, IdentityClaimTypeDto>());
		}
	}
}
