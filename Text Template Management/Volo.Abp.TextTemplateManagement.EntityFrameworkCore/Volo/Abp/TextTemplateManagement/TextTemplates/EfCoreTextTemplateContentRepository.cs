﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.TextTemplateManagement.EntityFrameworkCore;

namespace Volo.Abp.TextTemplateManagement.TextTemplates
{
    public class EfCoreTextTemplateContentRepository : EfCoreRepository<ITextTemplateManagementDbContext, TextTemplateContent, Guid>, IReadOnlyBasicRepository<TextTemplateContent, Guid>, IReadOnlyBasicRepository<TextTemplateContent>, IBasicRepository<TextTemplateContent>, IBasicRepository<TextTemplateContent, Guid>, IRepository, ITextTemplateContentRepository
	{
		public EfCoreTextTemplateContentRepository(IDbContextProvider<ITextTemplateManagementDbContext> dbContextProvider)
			: base(dbContextProvider)
		{
		}

		public virtual async Task<TextTemplateContent> GetAsync(string name, string cultureName = null)
		{
			return await this.DbSet.Where(x => x.Name.Equals(name)).Where(x => x.CultureName.Equals(cultureName)).FirstOrDefaultAsync();
		}

		public virtual async Task<TextTemplateContent> FindAsync(string name, string cultureName = null)
		{
			return await this.DbSet.Where(x => x.Name.Equals(name)).Where(x => x.CultureName.Equals(cultureName)).FirstOrDefaultAsync();
		}
	}
}
