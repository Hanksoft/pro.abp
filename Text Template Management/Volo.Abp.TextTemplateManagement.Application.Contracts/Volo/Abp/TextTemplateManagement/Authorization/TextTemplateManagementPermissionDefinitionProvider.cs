﻿using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;
using Volo.Abp.TextTemplateManagement.Localization;

namespace Volo.Abp.TextTemplateManagement.Authorization
{
    public class TextTemplateManagementPermissionDefinitionProvider : PermissionDefinitionProvider
	{
		public override void Define(IPermissionDefinitionContext context)
		{
			context.AddGroup(TextTemplateManagementPermissions.GroupName, L("Permission:TextTemplateManagement")).AddPermission(TextTemplateManagementPermissions.TextTemplates.Default, L("Permission:TextTemplates")).AddChild(TextTemplateManagementPermissions.TextTemplates.EditContents, L("Permission:EditContents"));
		}

		private static LocalizableString L(string name)
		{
			return LocalizableString.Create<TextTemplateManagementResource>(name);
		}
	}
}
