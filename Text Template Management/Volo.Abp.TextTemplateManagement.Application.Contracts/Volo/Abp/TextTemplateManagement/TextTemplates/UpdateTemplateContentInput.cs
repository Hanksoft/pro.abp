﻿using System.ComponentModel.DataAnnotations;
using Volo.Abp.Validation;

namespace Volo.Abp.TextTemplateManagement.TextTemplates
{
    public class UpdateTemplateContentInput
	{
		[DynamicStringLength(typeof(TextTemplateConsts), "MaxNameLength", null)]
		[Required]
		public string TemplateName { get; set; }

		[DynamicStringLength(typeof(TextTemplateConsts), "MaxCultureNameLength", null)]
		public string CultureName { get; set; }

		public string Content { get; set; }
	}
}
